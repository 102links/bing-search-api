let cluster = require('cluster');
if (cluster.isMaster) {
    let numWorkers = require('os').cpus().length;
    for (var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }
    cluster.on('online', function (worker) {
    });
    cluster.on('exit', function (worker, code, signal) {
        cluster.fork();
    });
}
else {
    require("./www");
}
//# sourceMappingURL=cluster.js.map