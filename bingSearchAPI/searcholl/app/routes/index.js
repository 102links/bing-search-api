'use strict';
const express = require('express');
const config_1 = require('../config/config');
const SearchFactory_1 = require("../models/SearchFactory");
const Pagination_1 = require("../models/Pagination");
const LoggerSingleton_1 = require("../models/LoggerSingleton");
const GeoSingleton_1 = require("../models/GeoSingleton");
const GoogleCodeSingleton_1 = require("../models/GoogleCodeSingleton");
let urlencode = require('urlencode');
let cacheManager = require('cache-manager');
const router = express.Router();
function getCachedResult(req, q, cb) {
    if (config_1.default.cache.enable) {
        var memoryCache = cacheManager.caching({ store: 'memory', max: 500, ttl: config_1.default.cache.timeout });
        var cacheId;
        let ip = (process.env.NODE_ENV === 'production') ?
            req.headers["x-real-ip"] :
            (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress);
        let geo = GeoSingleton_1.GeoSingleton.Instance.getGeo(ip);
        cacheId = 'q=' + q;
        cacheId += '&count=' + config_1.default.search.resultsNo;
        cacheId += '&offset=' + req.query.offset;
        if (geo) {
            cacheId += '&geo=' + geo.country;
        }
        memoryCache.get(cacheId, function (err, result) {
            if (err) {
                return cb(err);
            }
            if (result) {
                result.source = 'cache';
                return cb(null, result);
            }
            var searchObject = SearchFactory_1.SearchFactory.getSearchObject(SearchFactory_1.SearchFactory.SEARCH_TYPE_BING_V5);
            searchObject.search(req, q, function (err, result) {
                if (err) {
                    return cb(err);
                }
                memoryCache.set(cacheId, result);
                result.source = 'request';
                cb(null, result);
            });
        });
    }
    else {
        var searchObject = SearchFactory_1.SearchFactory.getSearchObject(SearchFactory_1.SearchFactory.SEARCH_TYPE_BING_V5);
        searchObject.search(req, q, function (err, result) {
            if (err) {
                return cb(err);
            }
            result.source = 'request';
            cb(null, result);
        });
    }
}
router.get('/', (req, res, next) => {
    let q = urlencode(req.query.q);
    let logger = LoggerSingleton_1.LoggerSingleton.Instance.getLogger();
    logger.log('debug', 'q: ', q);
    if (q && q != 'undefined' && q.length > 0) {
        next();
    }
    else {
        res.render('index', {
            title: 'Searcholl',
            GoogleCodeSingleton: GoogleCodeSingleton_1.GoogleCodeSingleton.Instance
        });
    }
});
router.get('/', (req, res, next) => {
    let q;
    let offset;
    res.locals.q = req.query.q;
    q = urlencode(req.query.q);
    req.query.offset = res.locals.offset = (req.query.offset) ? urlencode(req.query.offset) : config_1.default.search.resultsOffset;
    getCachedResult(req, q, (err, response) => {
        if (err) {
            console.error(err);
            res.locals.jsonResponse = null;
            res.locals.pagination = null;
            next();
            return;
        }
        var pagination = new Pagination_1.Pagination(res.locals.offset, config_1.default.search.resultsNo, response.response.results.totalMatches, config_1.default.search.pagesToDisplay);
        res.locals.jsonResponse = response;
        res.locals.pagination = pagination.getPaginationObject();
        next();
    });
});
router.get('/', (req, res, next) => {
    res.render('resultPage', {
        title: 'searcholl',
        q: res.locals.q,
        jsonResponse: res.locals.jsonResponse,
        pagination: res.locals.pagination,
        GoogleCodeSingleton: GoogleCodeSingleton_1.GoogleCodeSingleton.Instance
    });
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
//# sourceMappingURL=index.js.map