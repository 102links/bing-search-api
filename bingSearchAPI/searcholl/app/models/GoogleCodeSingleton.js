"use strict";
class GoogleCodeSingleton {
    constructor() {
        this.tagManager = "<!-- Google Tag Manager --> \n" +
            "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': \n" +
            "new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], \n" +
            "j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= \n" +
            "'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); \n" +
            "})(window,document,'script','dataLayer','GTM-N6MM6BN');</script> \n" +
            "<!-- End Google Tag Manager --> \n";
        this.tagManagerNoScript = "<!-- Google Tag Manager (noscript) --> \n" +
            "<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-N6MM6BN\" \n" +
            "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript> \n" +
            "<!-- End Google Tag Manager (noscript) -->  \n";
        this.tagManagerPrinted = false;
        this.tagManagerNoScriptPrinted = false;
    }
    static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new GoogleCodeSingleton();
        }
        return this.instance;
    }
    getTabManager() {
        if (!this.tagManagerPrinted && process.env.NODE_ENV == 'production') {
            this.tagManagerPrinted = true;
            return this.tagManager;
        }
        return '';
    }
    getTabManagerNoScript() {
        if (!this.tagManagerNoScriptPrinted && process.env.NODE_ENV == 'production') {
            this.tagManagerNoScriptPrinted = true;
            return this.tagManagerNoScript;
        }
        return '';
    }
}
exports.GoogleCodeSingleton = GoogleCodeSingleton;
//# sourceMappingURL=GoogleCodeSingleton.js.map