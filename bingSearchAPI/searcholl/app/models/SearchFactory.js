"use strict";
const SearchDecorator_1 = require("./SearchDecorator");
const BingSearchV5Adapter_1 = require("./BingSearchV5Adapter");
class SearchFactory {
    static getSearchObject(searchType) {
        if (searchType === SearchFactory.SEARCH_TYPE_BING_V5) {
            return new SearchDecorator_1.SearchDecorator(new BingSearchV5Adapter_1.BingSearchV5Adapter, SearchFactory.SEARCH_TYPE_BING_V5);
        }
        return null;
    }
}
SearchFactory.SEARCH_TYPE_BING_V5 = 'SEARCH_TYPE_BING_V5';
exports.SearchFactory = SearchFactory;
//# sourceMappingURL=SearchFactory.js.map