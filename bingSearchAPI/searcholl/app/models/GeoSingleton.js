"use strict";
let geoip = require('geoip-lite');
const LoggerSingleton_1 = require("../models/LoggerSingleton");
class GeoSingleton {
    constructor() {
    }
    generateGeo(ip) {
        let logger = LoggerSingleton_1.LoggerSingleton.Instance.getLogger();
        this.geo = geoip.lookup(ip);
        logger.log('debug', 'geo: ', this.geo);
    }
    static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new GeoSingleton();
        }
        return this.instance;
    }
    getGeo(ip) {
        if ((this.ip === null || this.ip === undefined) || this.ip != ip) {
            this.ip = ip;
            this.generateGeo(ip);
        }
        return this.geo;
    }
}
exports.GeoSingleton = GeoSingleton;
//# sourceMappingURL=GeoSingleton.js.map