"use strict";
const https = require('https');
const config_1 = require('../config/config');
const LoggerSingleton_1 = require("../models/LoggerSingleton");
const GeoSingleton_1 = require("../models/GeoSingleton");
class BingSearchV5 {
    static getHttpsRequestOptions(req, q) {
        let options;
        let logger = LoggerSingleton_1.LoggerSingleton.Instance.getLogger();
        let mk = config_1.default.bingSearch.mkt;
        let ip = (process.env.NODE_ENV === 'production') ?
            req.headers["x-real-ip"] :
            (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress);
        let geo = GeoSingleton_1.GeoSingleton.Instance.getGeo(ip);
        options = {
            hostname: BingSearchV5.BING_SEARCH_BASE_URL,
            port: 443,
            path: '/bing/v5.0/',
            method: 'GET',
            headers: {
                'User-Agent': req.headers['user-agent'],
                'Ocp-Apim-Subscription-Key': config_1.default.bing.key },
            'X-Search-ClientIP': ip,
            'X-MSEdge-ClientIP': ip,
            'textDecorations': true,
            'textFormat': 'HTML'
        };
        logger.log('debug', 'ip: ', ip);
        if (geo) {
            mk = 'en-' + geo.country;
            options['headers']['X-Search-Location'] = [];
            options['headers']['X-Search-Location'].push('lat:' + geo.ll[0]);
            options['headers']['X-Search-Location'].push('long:' + geo.ll[1]);
            options['headers']['X-Search-Location'].push('re:18,000m');
        }
        options.path += 'search?';
        options.path += 'q=' + q;
        options.path += '&count=' + config_1.default.search.resultsNo;
        options.path += '&offset=' + req.query.offset;
        options.path += '&mkt=' + mk;
        options.path += '&safesearch=' + config_1.default.bingSearch.safeSearch;
        logger.log('debug', 'search options sent to bing: ', options);
        return options;
    }
    search(req, q, callback) {
        let options;
        options = BingSearchV5.getHttpsRequestOptions(req, q);
        var httpsReq = https.request(options, (bingResponse) => {
            var bingResponseData = '';
            let logger = LoggerSingleton_1.LoggerSingleton.Instance.getLogger();
            logger.log('debug', 'return statusCode:', bingResponse.statusCode);
            logger.log('debug', 'return headers:', bingResponse.headers);
            bingResponse.on('data', (chunk) => {
                bingResponseData += chunk;
            });
            bingResponse.on('end', function () {
                logger.log('debug', 'bingResponseData length: ', bingResponseData.length);
                callback((bingResponse.statusCode == 200) ? null : 'error', JSON.parse(bingResponseData));
            });
        });
        httpsReq.on('error', (e) => {
            httpsReq.end();
            callback(e);
            return;
        });
        httpsReq.end();
    }
}
BingSearchV5.RESPONSE_TYPE_WEB_PAGE = 'WebPages';
BingSearchV5.RESPONSE_TYPE_NEWS = 'News';
BingSearchV5.RESPONSE_TYPE_IMAGES = 'Images';
BingSearchV5.RESPONSE_TYPE_VIDEOS = 'Videos';
BingSearchV5.RESPONSE_TYPE_RELATED_SEARCH = 'RelatedSearches';
BingSearchV5.RESPONSE_TYPE_QUERY_CONTEXT = 'queryContext';
BingSearchV5.RESPONSE_SEARCH_RESPONSE = 'SearchResponse';
BingSearchV5.BING_SEARCH_BASE_URL = 'api.cognitive.microsoft.com';
exports.BingSearchV5 = BingSearchV5;
//# sourceMappingURL=BingSearchV5.js.map