"use strict";
class SearchDecorator {
    constructor(component, searchType) {
        this.searchType = searchType;
        this.component = component;
    }
    search(req, q, callback) {
        this.component.search(req, q, (err, response) => {
            if (err) {
                callback(err);
                return;
            }
            let newRetrunValue = {
                'searchType': this.searchType,
                'response': response
            };
            callback(null, newRetrunValue);
        });
    }
}
exports.SearchDecorator = SearchDecorator;
//# sourceMappingURL=SearchDecorator.js.map