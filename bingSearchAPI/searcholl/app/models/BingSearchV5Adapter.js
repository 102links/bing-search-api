"use strict";
const BingSearchV5_1 = require("./BingSearchV5");
const AbstractSearch_1 = require("./AbstractSearch");
const SearchResults_1 = require("./SearchResults");
const WebPageResult_1 = require("./searchResult/WebPageResult");
const NewsResult_1 = require("./searchResult/NewsResult");
const ImageResult_1 = require("./searchResult/ImageResult");
const VideoResult_1 = require("./searchResult/VideoResult");
const RelatedSearch_1 = require("./searchResult/RelatedSearch");
const QueryContext_1 = require("./searchResult/QueryContext");
const LoggerSingleton_1 = require("../models/LoggerSingleton");
let urlencode = require('urlencode');
class BingSearchV5Adapter extends AbstractSearch_1.AbstractSearch {
    static getImages(images) {
        let response = [];
        for (let j = 0; j < images.length; j++) {
            let image = new ImageResult_1.ImageResult;
            image.type = {
                id: ImageResult_1.ImageResult.SEARCH_TYPE,
                label: ImageResult_1.ImageResult.SEARCH_LABEL
            };
            image.name = images[j].name;
            image.thumbnail = {};
            image.thumbnail.url = images[j].thumbnailUrl;
            image.thumbnail.width = images[j].thumbnail.width;
            image.thumbnail.height = images[j].thumbnail.height;
            image.datePublished = images[j].datePublished;
            image.image_url = images[j].contentUrl;
            image.url = images[j].hostPageDisplayUrl;
            response.push(image);
        }
        return response;
    }
    static getVideos(videos) {
        let response = [];
        for (let j = 0; j < videos.length; j++) {
            let video = new VideoResult_1.VideoResult;
            video.type = {
                id: VideoResult_1.VideoResult.SEARCH_TYPE,
                label: VideoResult_1.VideoResult.SEARCH_LABEL,
            };
            video.name = videos[j].name;
            video.snippet = videos[j].description;
            video.thumbnail = {};
            video.thumbnail.url = videos[j].thumbnailUrl;
            video.thumbnail.width = videos[j].thumbnail.width;
            video.thumbnail.height = videos[j].thumbnail.height;
            video.datePublished = videos[j].datePublished;
            video.video_url = videos[j].contentUrl;
            video.url = videos[j].hostPageDisplayUrl;
            video.publisher = videos[j].publisher.name;
            response.push(video);
        }
        return response;
    }
    static getWebPages(webPages) {
        let response = [];
        for (let j = 0; j < webPages.length; j++) {
            let webPage = new WebPageResult_1.WebPageResult;
            webPage.type = {
                id: WebPageResult_1.WebPageResult.SEARCH_TYPE,
                label: WebPageResult_1.WebPageResult.SEARCH_LABEL,
            };
            webPage.name = webPages[j].name;
            webPage.url = webPages[j].url;
            webPage.displayUrl = webPages[j].displayUrl;
            webPage.dateLastCrawled = webPages[j].dateLastCrawled;
            webPage.snippet = webPages[j].snippet;
            response.push(webPage);
        }
        return response;
    }
    static getNews(news) {
        let response = [];
        for (let j = 0; j < news.length; j++) {
            let _news = new NewsResult_1.NewsResult;
            _news.type = {
                id: NewsResult_1.NewsResult.SEARCH_TYPE,
                label: NewsResult_1.NewsResult.SEARCH_LABEL
            };
            _news.name = news[j].name;
            _news.url = news[j].url;
            _news.snippet = news[j].description;
            _news.datePublished = news[j].datePublished;
            _news.thumbnail = {};
            _news.thumbnail.url = (news[j].image) ? news[j].image.thumbnail.contentUrl : null;
            response.push(_news);
        }
        return response;
    }
    static addMainline(response) {
        let mainline = [];
        let logger = LoggerSingleton_1.LoggerSingleton.Instance.getLogger();
        for (let i = 0; i < response.rankingResponse.mainline.items.length; i++) {
            let item = response.rankingResponse.mainline.items[i];
            switch (item.answerType) {
                case BingSearchV5_1.BingSearchV5.RESPONSE_TYPE_WEB_PAGE:
                    if (item.hasOwnProperty('resultIndex')) {
                        mainline = mainline.concat(BingSearchV5Adapter.getWebPages([response.webPages.value[item.resultIndex]]));
                    }
                    else {
                        mainline = mainline.concat(BingSearchV5Adapter.getWebPages(response.webPages.value));
                    }
                    break;
                case BingSearchV5_1.BingSearchV5.RESPONSE_TYPE_IMAGES:
                    if (item.hasOwnProperty('resultIndex')) {
                        mainline = mainline.concat(BingSearchV5Adapter.getImages([response.images.value[item.resultIndex]]));
                    }
                    else {
                        mainline = mainline.concat(BingSearchV5Adapter.getImages(response.images.value));
                    }
                    break;
                case BingSearchV5_1.BingSearchV5.RESPONSE_TYPE_VIDEOS:
                    if (item.hasOwnProperty('resultIndex')) {
                        mainline = mainline.concat(BingSearchV5Adapter.getVideos([response.videos.value[item.resultIndex]]));
                    }
                    else {
                        mainline = mainline.concat(BingSearchV5Adapter.getVideos(response.videos.value));
                    }
                    break;
                case BingSearchV5_1.BingSearchV5.RESPONSE_TYPE_NEWS:
                    if (item.hasOwnProperty('resultIndex')) {
                        mainline = mainline.concat(BingSearchV5Adapter.getNews([response.news.value[item.resultIndex]]));
                    }
                    else {
                        mainline = mainline.concat(BingSearchV5Adapter.getNews(response.news.value));
                    }
                    break;
            }
        }
        return mainline;
    }
    static getRelatedSearch(relatedSearches) {
        let response = [];
        for (let j = 0; j < relatedSearches.length; j++) {
            let _relatedSearch = new RelatedSearch_1.RelatedSearch;
            _relatedSearch.type = {
                id: RelatedSearch_1.RelatedSearch.SEARCH_TYPE,
                label: RelatedSearch_1.RelatedSearch.SEARCH_LABEL
            };
            _relatedSearch.text = relatedSearches[j].text;
            _relatedSearch.displayText = relatedSearches[j].displayText;
            _relatedSearch.url = '/?q=' + urlencode(relatedSearches[j].text);
            response.push(_relatedSearch);
        }
        return response;
    }
    static addSidebar(response) {
        let sidebar = [];
        for (let i = 0; i < response.rankingResponse.sidebar.items.length; i++) {
            let item = response.rankingResponse.sidebar.items[i];
            switch (item.answerType) {
                case BingSearchV5_1.BingSearchV5.RESPONSE_TYPE_RELATED_SEARCH:
                    if (item.hasOwnProperty('resultIndex')) {
                        sidebar = sidebar.concat(BingSearchV5Adapter.getRelatedSearch([response.relatedSearches.value[item.resultIndex]]));
                    }
                    else {
                        sidebar = sidebar.concat(BingSearchV5Adapter.getRelatedSearch(response.relatedSearches.value));
                    }
                    break;
            }
        }
        return sidebar;
    }
    static queryContext(response) {
        let _queryContext = new QueryContext_1.QueryContext;
        _queryContext.type = {
            id: QueryContext_1.QueryContext.SEARCH_TYPE,
            label: QueryContext_1.QueryContext.SEARCH_LABEL
        };
        _queryContext.originalQuery = response.queryContext.originalQuery;
        _queryContext.alteredQuery = response.queryContext.alteredQuery;
        _queryContext.alterationOverrideQuery = response.queryContext.alterationOverrideQuery;
        _queryContext.overrideUrl = '/?q=' + urlencode(response.queryContext.alterationOverrideQuery);
        _queryContext.adultIntent = response.queryContext.adultIntent;
        return _queryContext;
    }
    getAdapedResponse(err, q, response, callback) {
        if (err) {
            callback(err);
            return;
        }
        var adaptedResponse = new SearchResults_1.SearchResults();
        if (response._type == BingSearchV5_1.BingSearchV5.RESPONSE_SEARCH_RESPONSE) {
            adaptedResponse.type = BingSearchV5Adapter.RESPONSE_OK;
            adaptedResponse.results = {
                totalMatches: 0,
                mainline: [],
                sidebar: [],
                queryContext: [],
                paginationBaseUrl: '/?q=' + q + '&offset='
            };
            if (response.rankingResponse && response.rankingResponse.mainline && response.rankingResponse.mainline.items) {
                adaptedResponse.results.mainline = adaptedResponse.results.mainline.concat(BingSearchV5Adapter.addMainline(response));
            }
            if (response.rankingResponse && response.rankingResponse.sidebar && response.rankingResponse.sidebar.items) {
                adaptedResponse.results.sidebar = adaptedResponse.results.sidebar.concat(BingSearchV5Adapter.addSidebar(response));
            }
            if (response.queryContext) {
                adaptedResponse.results.queryContext = BingSearchV5Adapter.queryContext(response);
            }
            if (response.webPages && response.webPages.totalEstimatedMatches) {
                adaptedResponse.results.totalMatches = response.webPages.totalEstimatedMatches;
            }
        }
        else {
            adaptedResponse.type = BingSearchV5Adapter.RESPONSE_ERROR;
            adaptedResponse.results = [];
            adaptedResponse.message = "Something went wrong...";
        }
        callback(err, adaptedResponse);
    }
    search(req, q, callback) {
        var adaptee = new BingSearchV5_1.BingSearchV5();
        adaptee.search(req, q, (err, response) => {
            this.getAdapedResponse(err, q, response, callback);
        });
    }
}
exports.BingSearchV5Adapter = BingSearchV5Adapter;
//# sourceMappingURL=BingSearchV5Adapter.js.map