"use strict";
class Pagination {
    constructor(offset, resultsNo, totalMatches, pagesToDisplay) {
        this.offset = offset;
        this.resultsNo = resultsNo;
        if (totalMatches) {
            this.totalMatches = totalMatches;
        }
        else {
            this.totalMatches = 0;
        }
        this.pagesToDisplay = pagesToDisplay;
    }
    getPaginationObject() {
        return {
            currentPage: this.getCurrentPage(),
            lastPage: this.getLastPage(),
            firstPage: this.getFirstPage(),
            previousPage: this.getPreviousPage(),
            nextPage: this.getNextPage(),
            resultsNo: this.getResultsNo()
        };
    }
    getCurrentPage() {
        if (this.currentPage) {
        }
        else {
            this.currentPage = Math.floor(this.offset / this.resultsNo);
        }
        return this.currentPage;
    }
    getResultsNo() {
        return this.resultsNo;
    }
    getLastPage() {
        if (this.lastPage) {
        }
        else {
            if (this.getCurrentPage() > Math.floor(this.pagesToDisplay / 2)) {
                this.lastPage = this.getCurrentPage() + Math.floor(this.pagesToDisplay / 2);
                if (this.lastPage > this.getMaxPages()) {
                    this.lastPage = this.getMaxPages();
                }
            }
            else {
                this.lastPage = this.pagesToDisplay;
            }
        }
        return this.lastPage;
    }
    getFirstPage() {
        if (this.firstPage) {
        }
        else {
            if (this.getCurrentPage() > Math.floor(this.pagesToDisplay / 2)) {
                this.firstPage = this.getCurrentPage() - Math.floor(this.pagesToDisplay / 2);
            }
            else {
                this.firstPage = 0;
            }
        }
        return this.firstPage;
    }
    getPreviousPage() {
        if (this.previous) {
        }
        else {
            if (this.getCurrentPage() > 0) {
                this.previous = this.getCurrentPage() - 1;
            }
            else {
                this.previous = null;
            }
        }
        return this.previous;
    }
    getNextPage() {
        if (this.next) {
        }
        else {
            if (this.getCurrentPage() < this.getMaxPages()) {
                this.next = this.getCurrentPage() + 1;
            }
            else {
                this.next = null;
            }
        }
        return this.next;
    }
    getMaxPages() {
        if (this.maxPages) {
        }
        else {
            this.maxPages = Math.ceil(this.totalMatches / this.resultsNo);
        }
        return this.maxPages;
    }
}
exports.Pagination = Pagination;
//# sourceMappingURL=Pagination.js.map