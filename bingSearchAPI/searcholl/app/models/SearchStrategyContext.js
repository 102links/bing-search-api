"use strict";
class SearchStrategyContext {
    constructor(searchStrategy) {
        this.searchStrategy = searchStrategy;
    }
    search(q) {
        return this.searchStrategy.getResults(q);
    }
}
exports.SearchStrategyContext = SearchStrategyContext;
//# sourceMappingURL=SearchStrategyContext.js.map