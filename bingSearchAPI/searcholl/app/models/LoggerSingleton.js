"use strict";
let winston = require('winston');
require('winston-daily-rotate-file');
const config_1 = require('../config/config');
class LoggerSingleton {
    constructor() {
        var fileTransport = new winston.transports.DailyRotateFile({
            filename: config_1.default.logger.path,
            datePattern: 'yyyy-MM-dd.HH.',
            prepend: true,
            level: process.env.NODE_ENV === 'development' ? 'debug' : 'info'
        });
        var consoleTransport = new winston.transports.Console({
            colorize: true,
            level: (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') ? 'debug' : 'info'
        });
        this.logger = new (winston.Logger)({
            transports: [
                fileTransport,
                consoleTransport
            ]
        });
    }
    static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new LoggerSingleton();
        }
        return this.instance;
    }
    getLogger() {
        return this.logger;
    }
}
exports.LoggerSingleton = LoggerSingleton;
//# sourceMappingURL=LoggerSingleton.js.map