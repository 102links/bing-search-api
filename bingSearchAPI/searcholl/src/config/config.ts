'use strict';

var envDetect = require('./json/env.config.json');

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') {
  let environmentFolderConfig = (envDetect) ? envDetect.env : 'default';

  var config = {
    general: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/general.json'),
    search: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/search.json'),
    bing: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/bing.json'),
    bingSearch: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/bingSearch.json'),
    logger: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/logger.json'),
    cache: require('./json/' + process.env.NODE_ENV + '/' + environmentFolderConfig + '/cache.json')
  }

} else if (process.env.NODE_ENV === 'production') {

  var config = {
    general: require('./json/production/general.json'),
    search: require('./json/production/search.json'),
    bing: require('./json/production/bing.json'),
    bingSearch: require('./json/production/bingSearch.json'),
    logger: require('./json/production/logger.json'),
    cache: require('./json/production/cache.json')
  }

} else {
  var config = {
    general: require('./json/production/general.json'),
    search: require('./json/production/search.json'),
    bing: require('./json/production/bing.json'),
    bingSearch: require('./json/production/bingSearch.json'),
    logger: require('./json/production/logger.json'),
    cache: require('./json/production/cache.json')
  }
}

export default config;
