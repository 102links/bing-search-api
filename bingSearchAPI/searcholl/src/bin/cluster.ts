let cluster = require('cluster');
 
/**
* A way to start the api with clusters to handle multiple cpu cores.
*/
 
if(cluster.isMaster){
  let numWorkers = require('os').cpus().length;
 
  // console.log('Master cluster setting up ' + numWorkers + ' workers...');
 
  for(var i = 0; i < numWorkers; i++) {
    cluster.fork();
  }
 
  cluster.on('online', function(worker) {
    // console.log('Worker ' + worker.process.pid + ' is online');
  });
 
  cluster.on('exit', function(worker, code, signal) {
    // ApiServices.errorLogger.log(new ApiServices.Err('Worker ' + worker.process.pid + ' died with code: '
    //   + code + ', and signal: ' + signal));
    // console.log('Worker ' + worker.process.pid + ' died with code: ' + code +
    //   ', and signal: ' + signal);
    // console.log('Starting a new worker');
    cluster.fork();
  });
}else{
  require("./www");
}