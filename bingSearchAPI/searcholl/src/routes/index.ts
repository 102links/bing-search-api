'use strict';

import * as express from 'express';

import {default as config} from '../config/config';

import { SearchFactory } from "../models/SearchFactory";

import { Pagination } from "../models/Pagination";

import { LoggerSingleton } from "../models/LoggerSingleton";
import { GeoSingleton } from "../models/GeoSingleton";
import { GoogleCodeSingleton } from "../models/GoogleCodeSingleton";

let urlencode = require('urlencode');

let cacheManager = require('cache-manager');

const router = express.Router();

function getCachedResult(req, q, cb) {
  if (config.cache.enable) {
    var memoryCache = cacheManager.caching({store: 'memory', max: 500, ttl: config.cache.timeout/*seconds*/});
    var cacheId;
    let ip = (process.env.NODE_ENV === 'production')?
        req.headers["x-real-ip"]:
          (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress);

    let geo = GeoSingleton.Instance.getGeo(ip);

    cacheId = 'q=' + q;
    cacheId += '&count=' + config.search.resultsNo;
    cacheId += '&offset=' + req.query.offset;
    if (geo) {
      cacheId += '&geo=' + geo.country;
    }

    memoryCache.get(cacheId, function (err, result) {
      if (err) { return cb(err); }

      if (result) {
        result.source = 'cache';
        return cb(null, result);
      }

      var searchObject = SearchFactory.getSearchObject(SearchFactory.SEARCH_TYPE_BING_V5);
      searchObject.search(req, q, function (err, result) {
        if (err) { return cb(err); }
        memoryCache.set(cacheId, result);
        result.source = 'request';
        cb(null, result);
      });
    });
  } else {
      var searchObject = SearchFactory.getSearchObject(SearchFactory.SEARCH_TYPE_BING_V5);
      searchObject.search(req, q, function (err, result) {
        if (err) { return cb(err); }
        result.source = 'request';
        cb(null, result);
      });
  }
}

router.get('/',(req,res,next) => {
  let q = urlencode(req.query.q);

  let logger = LoggerSingleton.Instance.getLogger();
  logger.log('debug', 'q: ', q);

  if (q && q != 'undefined' && q.length > 0) {
    next();
  } else {
    res.render('index', {
      title: 'Searcholl',
      GoogleCodeSingleton: GoogleCodeSingleton.Instance
    });
  }
});


/* GET home page. */
router.get('/',(req, res, next) => {
  let q;
  let offset;

  res.locals.q = req.query.q;
  q = urlencode(req.query.q);
  req.query.offset = res.locals.offset = (req.query.offset)?urlencode(req.query.offset):config.search.resultsOffset;

  getCachedResult(req, q, (err, response) => {
    if (err) {
      console.error(err);
      // search the next search provider
      res.locals.jsonResponse = null;
      res.locals.pagination = null;
      next(); return;
    }

    var pagination = new Pagination(res.locals.offset, config.search.resultsNo, response.response.results.totalMatches, config.search.pagesToDisplay);

    res.locals.jsonResponse = response;
    res.locals.pagination = pagination.getPaginationObject();
    next();
  });
});

router.get('/',(req,res,next) => {
  res.render('resultPage', {
    title: 'searcholl',
    q: res.locals.q,
    jsonResponse: res.locals.jsonResponse,
    pagination: res.locals.pagination,
    GoogleCodeSingleton: GoogleCodeSingleton.Instance
  });
});

export default router;
