import { AbstractSearchInterface } from "./AbstractSearchInterface";

export abstract class AbstractSearch implements AbstractSearchInterface {

  public static readonly RESPONSE_OK = 'OK';
  public static readonly RESPONSE_ERROR = 'ERROR';

  abstract search(req:Object, q:string, callback: Function);
}
