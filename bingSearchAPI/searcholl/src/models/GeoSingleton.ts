let geoip = require('geoip-lite');
import { LoggerSingleton } from "../models/LoggerSingleton";

export class GeoSingleton {
  private static instance: GeoSingleton;

  private geo;
  private ip;

  constructor() {
  }

  private generateGeo(ip) {
    let logger = LoggerSingleton.Instance.getLogger();

    this.geo = geoip.lookup(ip);

    logger.log('debug', 'geo: ', this.geo);
  }

  static get Instance() {
    if (this.instance === null || this.instance === undefined) {
      this.instance = new GeoSingleton();
    }
    return this.instance;
  }

  public getGeo(ip) {
    if ((this.ip === null || this.ip === undefined) || this.ip != ip) {
      this.ip = ip;
      this.generateGeo(ip);
    }
    return this.geo;
  }

}
