import { AbstractSearchInterface } from "./AbstractSearchInterface";

import * as https from 'https';
import {default as config} from '../config/config';
import { LoggerSingleton } from "../models/LoggerSingleton";
import { GeoSingleton } from "../models/GeoSingleton";

export class BingSearchV5 implements AbstractSearchInterface {

  public static readonly RESPONSE_TYPE_WEB_PAGE = 'WebPages';
  public static readonly RESPONSE_TYPE_NEWS = 'News';
  public static readonly RESPONSE_TYPE_IMAGES = 'Images';
  public static readonly RESPONSE_TYPE_VIDEOS = 'Videos';
  public static readonly RESPONSE_TYPE_RELATED_SEARCH = 'RelatedSearches';
  public static readonly RESPONSE_TYPE_QUERY_CONTEXT = 'queryContext';


  public static readonly RESPONSE_SEARCH_RESPONSE = 'SearchResponse';

  public static readonly BING_SEARCH_BASE_URL = 'api.cognitive.microsoft.com';

  private static getHttpsRequestOptions(req, q:string): string{
    let options;
    let logger = LoggerSingleton.Instance.getLogger();
    let mk = config.bingSearch.mkt;
    let ip = (process.env.NODE_ENV === 'production')?
        req.headers["x-real-ip"]:
          (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress);

    let geo = GeoSingleton.Instance.getGeo(ip);

    options = {
      hostname: BingSearchV5.BING_SEARCH_BASE_URL,
      port: 443,
      path: '/bing/v5.0/',
      method: 'GET',
      headers: {
        'User-Agent': req.headers['user-agent'],
        'Ocp-Apim-Subscription-Key': config.bing.key},
        'X-Search-ClientIP': ip,
        'X-MSEdge-ClientIP': ip,
        'textDecorations': true,
        'textFormat': 'HTML'
    }

    logger.log('debug', 'ip: ', ip);

    if (geo) {
      mk = 'en-' + geo.country;
      options['headers']['X-Search-Location'] = [];
      options['headers']['X-Search-Location'].push('lat:' + geo.ll[0]);
      options['headers']['X-Search-Location'].push('long:' + geo.ll[1]);
      options['headers']['X-Search-Location'].push('re:18,000m');
    }

    options.path += 'search?';
    options.path += 'q=' + q;
    options.path += '&count=' + config.search.resultsNo;
    options.path += '&offset=' + req.query.offset;
    options.path += '&mkt=' + mk;
    options.path += '&safesearch=' + config.bingSearch.safeSearch;

    logger.log('debug', 'search options sent to bing: ', options);

    return options;
  }

  public search(req:Object, q:string, callback) {
    let options;

    options = BingSearchV5.getHttpsRequestOptions(req, q);

    var httpsReq = https.request(options, (bingResponse) => {
      var bingResponseData = '';

      let logger = LoggerSingleton.Instance.getLogger();
      logger.log('debug', 'return statusCode:', bingResponse.statusCode);
      logger.log('debug', 'return headers:', bingResponse.headers);

      bingResponse.on('data', (chunk) => {
        bingResponseData += chunk;
      });

      bingResponse.on('end', function () {
        logger.log('debug', 'bingResponseData length: ', bingResponseData.length);
        //to do: buld a more relevant respons error
        callback((bingResponse.statusCode == 200)?null:'error', JSON.parse(bingResponseData));
      });
    });

    httpsReq.on('error', (e) => {
      httpsReq.end();
      callback(e); return;
    });

    httpsReq.end();
  }

}
