 export class Pagination {
  private offset:number;
  private resultsNo:number;
  private totalMatches:number;
  private pagesToDisplay:number;

  private currentPage:number;
  private lastPage:number;
  private firstPage:number;
  private previous:number;
  private next:number;

  private maxPages:number;

  constructor(offset, resultsNo, totalMatches, pagesToDisplay) {
    this.offset = offset;
    this.resultsNo = resultsNo;
    if (totalMatches) {
      this.totalMatches = totalMatches;
    } else {
      this.totalMatches = 0;
    }
    this.pagesToDisplay = pagesToDisplay;
  }

  public getPaginationObject() {
    return {
      currentPage: this.getCurrentPage(),
      lastPage: this.getLastPage(),
      firstPage: this.getFirstPage(),
      previousPage: this.getPreviousPage(),
      nextPage: this.getNextPage(),
      resultsNo: this.getResultsNo()
    }
  }

  public getCurrentPage() {
    if (this.currentPage) {
    } else {
      this.currentPage = Math.floor(this.offset / this.resultsNo);
    }
    return this.currentPage;
  }

  public getResultsNo() {
    return this.resultsNo;
  }

  public getLastPage() {
    if (this.lastPage) {
    } else {
      if(this.getCurrentPage() > Math.floor(this.pagesToDisplay / 2)) {
        this.lastPage = this.getCurrentPage() + Math.floor(this.pagesToDisplay / 2);
        if (this.lastPage > this.getMaxPages()) {
          this.lastPage = this.getMaxPages();
        }
      } else {
        this.lastPage = this.pagesToDisplay;
      }
    }
    return this.lastPage;
  }

  public getFirstPage() {
    if (this.firstPage) {
    } else {
      if(this.getCurrentPage() > Math.floor(this.pagesToDisplay / 2)) {
        this.firstPage = this.getCurrentPage() - Math.floor(this.pagesToDisplay / 2);
      } else {
        this.firstPage = 0;
      }
    }
    return this.firstPage;
  }

  public getPreviousPage() {
    if (this.previous) {
    } else {
      if(this.getCurrentPage() > 0) {
        this.previous = this.getCurrentPage() - 1;
      } else {
        this.previous = null;
      }
    }
    return this.previous;
  }

  public getNextPage() {
    if (this.next) {
    } else {
      if(this.getCurrentPage() < this.getMaxPages()) {
        this.next = this.getCurrentPage() + 1;
      } else {
        this.next = null;
      }
    }
    return this.next;
  }

  private getMaxPages() {
    if (this.maxPages) {
    } else {
      this.maxPages = Math.ceil(this.totalMatches / this.resultsNo);
    }
    return this.maxPages;
  }
}
