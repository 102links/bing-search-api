export interface AbstractSearchInterface {
  search(req:Object, q:string, callback: Function);
}
