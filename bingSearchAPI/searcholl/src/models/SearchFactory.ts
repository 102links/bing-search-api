import { AbstractSearchInterface } from "./AbstractSearchInterface";
import { SearchDecorator } from "./SearchDecorator";

import { BingSearchV5Adapter } from "./BingSearchV5Adapter";

 export class SearchFactory {
        public static readonly SEARCH_TYPE_BING_V5 = 'SEARCH_TYPE_BING_V5';

        public static getSearchObject(searchType: string) : AbstractSearchInterface {
          if (searchType === SearchFactory.SEARCH_TYPE_BING_V5) {
            return new SearchDecorator(new BingSearchV5Adapter, SearchFactory.SEARCH_TYPE_BING_V5);
          }

          return null;
        }
}
