let winston = require('winston');
require('winston-daily-rotate-file');

import {default as config} from '../config/config';

export class LoggerSingleton {
  private static instance: LoggerSingleton;

  private logger;

  constructor() {
    var fileTransport = new winston.transports.DailyRotateFile({
      filename: config.logger.path,
      datePattern: 'yyyy-MM-dd.HH.',
      prepend: true,
      level: process.env.NODE_ENV === 'development' ? 'debug' : 'info'
    });

    var consoleTransport = new  winston.transports.Console({
      colorize: true,
      level: (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'testing') ? 'debug' : 'info'
    });

    this.logger = new (winston.Logger)({
      transports: [
        fileTransport,
        consoleTransport
      ]
    });
  }

  static get Instance() {
    if (this.instance === null || this.instance === undefined) {
      this.instance = new LoggerSingleton();
    }
    return this.instance;
  }

  public getLogger() {
    return this.logger;
  }

}
