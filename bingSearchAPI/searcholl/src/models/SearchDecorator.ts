import { AbstractSearchInterface } from "./AbstractSearchInterface";

export class SearchDecorator implements AbstractSearchInterface {
  private component: AbstractSearchInterface;
  private searchType: string;

  constructor(component: AbstractSearchInterface, searchType: string) {
    this.searchType = searchType;
    this.component = component;
  }

  public search(req:Object, q: string, callback: Function) {
    this.component.search(req, q, (err, response) => {
      if (err) {
        callback(err); return;
      }

      let newRetrunValue = {
        'searchType': this.searchType,
        'response': response
      }

      callback(null, newRetrunValue);
    });
  }
}
