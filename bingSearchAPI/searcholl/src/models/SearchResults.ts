export class SearchResults {
  public type: String;
  public source: String;
  public results;
  public message: String;
}
