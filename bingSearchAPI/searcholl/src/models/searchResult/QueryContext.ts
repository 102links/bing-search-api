import { SearchResult } from "./SearchResult";

export class QueryContext implements SearchResult{
  public static readonly SEARCH_TYPE = 'queryContext';
  public static readonly SEARCH_LABEL = 'Context';
  public type;

  public originalQuery: String;
  public alteredQuery: String;
  public alterationOverrideQuery: String;
  public adultIntent: boolean;
  public overrideUrl: String;
}
