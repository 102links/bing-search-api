import { SearchResult } from "./SearchResult";

export class RelatedSearch implements SearchResult{
  public static readonly SEARCH_TYPE = 'relatedSearches';
  public static readonly SEARCH_LABEL = 'Related Search';
  public type;

  public text: String;
  public displayText: String;
  public url: String;
}
