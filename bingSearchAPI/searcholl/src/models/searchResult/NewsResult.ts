import { SearchResult } from "./SearchResult";

export class NewsResult implements SearchResult{
  public static readonly SEARCH_TYPE = 'news';
  public static readonly SEARCH_LABEL = 'In News';
  public type;

  public name: String;
  public url: URL;
  public thumbnail;
  public snippet: String;
  public datePublished;
}
