import { SearchResult } from "./SearchResult";

export class ImageResult implements SearchResult{
  public static readonly SEARCH_TYPE = 'image';
  public static readonly SEARCH_LABEL = 'In Images';
  public type;

  public name: String;
  public thumbnail;
  public datePublished;
  public image_url: URL;
  public url: URL;
  public snippet: String;
}
