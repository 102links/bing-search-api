import { SearchResult } from "./SearchResult";

export class WebPageResult implements SearchResult{
  public static readonly SEARCH_TYPE = 'webPage';
  public static readonly SEARCH_LABEL = 'Webpages';
  public type;

  public name: String;
  public url: URL;
  public displayUrl: URL;
  public dateLastCrawled;
  public snippet: String;
  public deepLinks: WebPageResult;
}
