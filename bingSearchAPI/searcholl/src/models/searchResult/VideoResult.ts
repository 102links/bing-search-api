import { SearchResult } from "./SearchResult";

export class VideoResult implements SearchResult{
  public static readonly SEARCH_TYPE = 'video';
  public static readonly SEARCH_LABEL = 'In Videos';
  public type;

  public name: String;
  public thumbnail;
  public datePublished;
  public video_url: URL;
  public url: URL;
  public snippet: String;
  public publisher: String;
}
