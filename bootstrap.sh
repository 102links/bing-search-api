echo "sudo apt-get update"
sudo apt-get update

echo "curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -"
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
echo "sudo apt-get install -y nodejs"
sudo apt-get install -y nodejs
echo "sudo -i npm install -g npm"
sudo -i npm install -g npm


echo "npm install -g yo typescript"
npm install -g yo typescript
echo "npm install -g generator-express-ts"
npm install -g generator-express-ts

echo "cd /bingSearchAPI"
cd /bingSearchAPI/searcholl

echo "Don't forget: npm install"

// https://www.npmjs.com/package/generator-express-ts
echo "https://www.npmjs.com/package/generator-express-ts"
echo "Generate your new project: "
echo "  yo express-ts"
echo "After generating your project, cd into it, compile TypeScript files into JavaScript files:"
echo "  tsc"
echo "After everything is finished, run your project:"
echo "  node app/bin/www.js --harmonytsc"
echo "  node app/bin/cluster.js --harmonytsc"
echo "  NODE_ENV=development  node app/bin/www.js --harmonytsc"
echo "  NODE_ENV=development  node app/bin/cluster.js --harmonytsc"
echo ""
# echo "https://github.com/remy/nodemon#nodemon"
echo "sudo -i npm install -g nodemon"
npm install -g nodemon
echo "  nodemon app/bin/www.js --harmonytsc"
echo "  nodemon app/bin/cluster.js --harmonytsc"
echo "  NODE_ENV=development  nodemon app/bin/www.js --harmonytsc"
echo "  NODE_ENV=development  nodemon app/bin/cluster.js --harmonytsc"
echo "https://www.npmjs.com/package/pm2"
echo "sudo -i npm install pm2 -g"
sudo -i npm install pm2 -g
# https://github.com/nodejs/node-v0.x-archive/issues/3911
# ln -s /usr/bin/nodejs /usr/bin/node
echo "  pm2 start app/bin/www.js"
echo "  pm2 start app/bin/www.js -i 4"
echo "  pm2 start app/bin/www.js --watch"
echo "  NODE_ENV=development   pm2 start app/bin/www.js"
echo "  NODE_ENV=development   pm2 start app/bin/www.js -i 4"
echo "  NODE_ENV=development   pm2 start app/bin/www.js --watch"
echo ""
echo "  sudo apt-get install git"
sudo apt-get install -y git
# echo "angular2 webpack example: "
# echo "  webpack – angular start project"
# echo "  https://github.com/preboot/angular2-webpack"
# echo "  git clone https://github.com/preboot/angular2-webpack.git angular2-webpack-example"
# # git clone https://github.com/preboot/angular2-webpack.git angular2-webpack-example
# echo"   npm install"
# echo"   npm start"
# echo "  go to http://localhost:8080 in your browser."
echo ""

# echo "http://furtive.co/"
# echo "  furtive CSS framework"
# # echo "  npm install --save furtive"
# # npm install --save furtive
# # echo "  bower install --save furtive"
# # bower install --save furtive
# echo  " git clone https://github.com/johnotander/furtive"
# git clone https://github.com/johnotander/furtive

# echo "npm install --save cache-manager"
# npm install --save cache-manager

# https://github.com/bluesmoon/node-geoip

echo "gem install sass";
gem install sass
